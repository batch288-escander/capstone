const express = require('express');

const auth = require('../auth.js')
const productsControllers = require('../controllers/productsControllers.js');

const router = express.Router();

// products Routes
router.post('/add', [auth.verify, productsControllers.addProduct]);
router.get('/all', [auth.verify, productsControllers.getAllProducts]);
router.get('/active', productsControllers.getActiveProducts);

// products/:productId Routes
router.get('/:productId', productsControllers.getProduct);
router.patch('/:productId', [auth.verify, productsControllers.updateProduct])
router.patch('/:productId/archive', [auth.verify, productsControllers.archiveProduct])
router.patch('/:productId/activate', [auth.verify, productsControllers.activateProduct])
router.post('/:productId/addToCart', [auth.verify, productsControllers.addToCart]);

module.exports = router;