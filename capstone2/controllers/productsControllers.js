const auth = require('../auth.js')
const Products = require('../models/Products.js');
const Users = require('../models/Users.js');

module.exports.addProduct = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    if(userData.isAdmin){
        Products.findOne({name: request.body.name})
        .then(result => {
            if(!result) {
                let newProduct = new Products({
                    name: request.body.name,
                    description: request.body.description,
                    price: request.body.price
                })

                newProduct.save()
                    .then(() => response.send(true))
                    .catch(error => response.send(false))
            } else {
                return response.send(false)
            }
        }).catch(error => response.send(false))
    }else{
        return response.send(false)
    }
    
}

module.exports.getAllProducts = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    if(userData.isAdmin){
        Products.find()
        .then(results => response.send(results))
        .catch(error => response.send(false))
    } else {
        response.send(false)
    }
}

module.exports.getActiveProducts = (request, response) => {
    Products.find({ isActive: true })
        .then(results => response.send(results))
        .catch(error => response.send(false))
}

module.exports.getProduct = (request, response) => {
    Products.findById(request.params.productId)
        .then(result => response.send(result))
        .catch(error => response.send(false))
}

module.exports.updateProduct = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    if(userData.isAdmin){
        Products.findByIdAndUpdate(request.params.productId, {
            name: request.body.name,
            description: request.body.description,
            price: request.body.price
        }, {new:true})
        .then(result => response.send(true))
        .catch(error => response.send(false))
    }else{
        return response.send(false)
    }
}

module.exports.archiveProduct = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    if(userData.isAdmin){
        Products.findByIdAndUpdate(request.params.productId, {
            isActive: false
        }, {new:true})
        .then(result => response.send(true))
        .catch(error => response.send(false))
    }else{
        return response.send(false)
    }
}

module.exports.activateProduct = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    if(userData.isAdmin){
        Products.findByIdAndUpdate(request.params.productId, {
            isActive: true
        }, {new:true})
        .then(result => response.send('Product is now active!'))
        .catch(error => response.send(error))
    }else{
        return response.send("You are not an Admin!")
    }
}


module.exports.addToCart = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    const productId = request.params.productId

    if(!userData.isAdmin){
        Users.findById(userData.id)
        .then(result => {
            const index = result.cartItems.map(item => item.product.toString()).indexOf(productId)
            if(index >= 0){
                result.cartItems[index].quantity += request.body.quantity
                result.cartItems[index].subtotal = result.cartItems[index].quantity * request.body.price
                result.save()
                .then(() => response.send("Item added to cart!"))
                .catch(error => response.send(error))
            } else {
                let newCartItem = {
                    product: productId,
                    quantity: request.body.quantity,
                    price: request.body.price,
                    subtotal: request.body.price * request.body.quantity
                }
                result.cartItems.push(newCartItem)

                result.save()
                .then(() => response.send("Item added to cart!"))
                .catch(error => response.send(error))
            }
        }).catch(error => response.send(error))
    } else {
        return response.send("Admins are not allowed to checkout items!")
    }
}

