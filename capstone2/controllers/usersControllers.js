const bcrypt = require('bcrypt');

const Users = require('../models/Users.js');
const auth = require('../auth.js')

module.exports.registerUser = (request, response) => {
    Users.findOne({ email: request.body.email})
        .then(result => {
            if(!result){
                let newUser = new Users({
                    firstName: request.body.firstName,
                    lastName: request.body.lastName,
                    email: request.body.email,
                    isSubscriber: request.body.isSubscriber,
                    password: bcrypt.hashSync(request.body.password, 10)
                });

                newUser.save()
                    .then(() => response.send(true))
                    .catch(error => response.send(false))

            } else {
                response.send(false)
            }
        }).catch(error => response.send(false))
}

module.exports.loginUser = (request, response) => {
    Users.findOne({ email:request.body.email})
        .then(result => {
            if(!result){
                response.send(false)
            } else {
                const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
                if(isPasswordCorrect){
                    return response.send({
                        auth: auth.createAccessToken(result)
                    })
                } else {
                    return response.send(false)
                }
            }
        })
        .catch(() => response.send(false))
}

module.exports.getUserDetails = ( request, response ) => {
    const userData = auth.decode(request.headers.authorization);
    Users.findById(userData.id)
    .then(result => {
        result.password ="***Password Encrypted***"
        response.send(result)
    }).catch(() => response.send(false))
}
module.exports.getProfile = ( request, response ) => {
    const userData = auth.decode(request.headers.authorization);
    if(userData.isAdmin) {
        Users.findById(request.body.id)
        .then(result => {
            if(!result){
                return response.send("User ID not found!");
            } else {
                result.password ="***Password Encrypted***"
                response.send(result)
            }
        }).catch(error => response.send(error))
    } else {
        Users.findById(userData.id)
        .then(result => {
            result.password ="***Password Encrypted***"
            response.send(result)
        }).catch(error => response.send(error))
    }
}

module.exports.makeAdmin = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    if(userData.isAdmin) {
        Users.findByIdAndUpdate(request.body.id,{ isAdmin: true })
        .then(result => {
            if(!result){
                return response.send("User ID not found!");
            } else {
                response.send(`User with email:${result.email} has been granted admin access`)
            }
        }).catch(error => response.send(error))
    } else {
        return response.send("You are not an Admin!");
    }
}

module.exports.removeAdmin = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    if(userData.isAdmin) {
        Users.findByIdAndUpdate(request.body.id,{ isAdmin: false })
        .then(result => {
            if(!result){
                return response.send("User ID not found!");
            } else {
                response.send(`User with email:${result.email} has been revoked admin access`)
            }
        }).catch(error => response.send(error))
    } else {
        return response.send("You are not an Admin!");
    }
}

// CART FUNCTIONS 

module.exports.viewCartItems = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    if(!userData.isAdmin){
        Users.findById(userData.id, 
            {   
                'cartItems': 1,
            }
        ).populate('cartItems.product', 'name')
        .then(result => response.send(result))
        .catch(error => response.send(error))

    } else {
        return response.send("Admins have no access on this route");
    }
}

module.exports.changeCartItemQty = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    if(!userData.isAdmin){
        Users.findById(userData.id)
        .then(result => {
            const index = result.cartItems.map(item => item.product.toString()).indexOf(request.body.product)
            result.cartItems[index].quantity = request.body.quantity
            result.cartItems[index].subtotal = result.cartItems[index].quantity * result.cartItems[index].price
            result.save()
            .then(() => response.send("Item Quantity updated in cart!"))
            .catch(error => response.send(error))
        }).catch(error => response.send(error))
    } else {
        return response.send("Admins are not allowed to checkout items!")
    }
}

module.exports.removeCartItem = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    if(!userData.isAdmin){
        Users.findById(userData.id)
        .then(result => {
            result.cartItems = result.cartItems.filter(item => item.product.toString() !== request.body.product)
            result.save()
            .then(() => response.send("Item successfully removed in the cart"))
            .catch(error => response.send(error))
        }).catch(error => response.send(error))
    } else {
        return response.send("Admins are not allowed to checkout items!")
    }
}


module.exports.cartSubtotals = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    if(!userData.isAdmin){
        Users.findById(userData.id, 
            {   
                'cartItems.product': 1,
                'cartItems.subtotal': 1
            }
        ).populate('cartItems.product', 'name')
        .then(result => response.send(result))
        .catch(error => response.send(error))

    } else {
        return response.send("Admins have no access on this route");
    }
}


module.exports.cartTotal = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    if(!userData.isAdmin){
        Users.findById(userData.id)
        .then(result => {
            const total = result.cartItems.reduce((sum,item) => {
                return item.subtotal + sum
            },0)

            return response.send(`Cart Total:${total}`)
        })
        .catch(error => response.send(error))

    } else {
        return response.send("Admins have no access on this route");
    }
}