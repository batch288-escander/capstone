const bcrypt = require("bcrypt");

const auth = require("../auth.js");
const Orders = require("../models/Orders.js");
const Users = require("../models/Users.js");

module.exports.checkOut = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    if(!userData.isAdmin){
        console.log('a')
        let total = request.body.products.reduce((sum, item) => {
            return sum + item.subtotal
        }, 0)
        console.log('b')
        let newOrder = new Orders({
            userId: userData.id,
            products: request.body.products,
            totalAmount: total
        })
        console.log('c')
        newOrder.save()
        .then(result => response.send(true))
        .catch(error => response.send(false))
    } else {
        return response.send(false)
    }
}


// module.exports.checkOut = (request, response) => {
//     const userData = auth.decode(request.headers.authorization);
//     if(!userData.isAdmin){
//         Users.findById(userData.id)
//         .then(result => {
//             if(result.cartItems.length > 0){
//                 let total = result.cartItems.reduce((sum, item) => {
//                     return sum + item.subtotal
//                 }, 0)
              
//                 let newOrder = new Orders({
//                     userId: userData.id,
//                     products: result.cartItems,
//                     totalAmount: total
//                 })
//                 newOrder.save()
//                 .then(result => {
                    
//                     Users.findByIdAndUpdate(userData.id, {
//                         cartItems: []
//                     }).then(result => response.send('Check out completed!'))
//                     .catch(error => response.send(error))
//                 })
//                 .catch(error => response.send(error))
//             }else{
//                 return response.send(`No items in cart yet, please add one before checking out!`)
//             }

//         }).catch(error => response.send(error))



        
//     } else {
//         return response.send("Admins are not allowed to checkout items!")
//     }
// }

module.exports.getOrders = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    if(!userData.isAdmin){
        Orders.find({ userId: userData.id })
        .then(results => response.send(results))
        .catch(error => response.send(error))
    }else{
        return response.send("Admins have no access on this route!")
    }

}

module.exports.getAllOrders = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    if(userData.isAdmin){
        Orders.find().populate('userId',['email']).populate('products.productID',['name'])
        .then(results => response.send(results))
        .catch(error => response.send(false))
    }else{
        return response.send(false)
    }

}